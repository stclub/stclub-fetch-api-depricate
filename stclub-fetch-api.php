<?php
/**
Plugin Name: stclub-fetch-api
description: show data in table using api
Version: 1.0
Author: Nadeem bhatti
License: later
Text Domain: stclub-fetch-api
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Starts the plugin.
 *
 * @since 1.0.0
 */
 add_action( 'rest_api_init', 'my_register_route' );

function my_register_route() {
    register_rest_route( 'api', 'all-user', array(
                    'methods' => 'GET',
                    'callback' => 'api_data',
                )
            );
}

// Enqueue your JS file
function load_my_scripts(){
	
    wp_register_style( 'table_style', plugin_dir_url( __FILE__ ) . 'css/api_style.css' );
    wp_enqueue_style( 'table_style');
    wp_enqueue_script( 'my-ajax-handle', plugin_dir_url( __FILE__ ) . 'js/my-ajax-request.js', array( 'jquery' ) );
    
}
add_action( 'wp_enqueue_scripts', 'load_my_scripts' );
function api_data() {
	$stc_api_info = get_transient( 'stc_info' );
 
	if( false === $stc_api_info ) {
		
	    // Transient expired, refresh the data
	    $request = wp_remote_get('https://jsonplaceholder.typicode.com/users');
		$body = wp_remote_retrieve_body( $request );
		$data = json_decode($body, true);
		set_transient( 'stc_info', $body, 60 );
		
	}else {
		
		$data = json_decode($stc_api_info, true);
	}
	
	return rest_ensure_response($data);
}


add_filter('the_content', 'endpoint_button');

function endpoint_button() {
    echo '<button type="button" id="callapi" class="btn btn-info">End Point</button>';
}
add_filter('the_content', 'end_point_api');
function end_point_api() {
    echo '<div id="response_area"></div>
          <div id="loader" style="display:none;">
		  </div>
          <div id="user_info"></div>';
}
add_action( 'wp_ajax_end_point_api', 'end_point_api' );
add_action( 'wp_ajax_nopriv_end_point_api', 'end_point_api' );